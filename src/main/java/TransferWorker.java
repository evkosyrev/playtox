import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

public class TransferWorker implements Runnable {
    private final Account a;
    private final Account b;
    private volatile AtomicInteger counter;
    private static final Logger logger = Logger.getLogger(TransferWorker.class);

    TransferWorker(Account a, Account b, AtomicInteger counter) {
        this.a = a;
        this.b = b;
        this.counter = counter;
    }

    public void run() {
        try {
            while (true) {
                int cnt = counter.incrementAndGet();
                if (cnt > 30) return;
                int transactionAmount = (int) (10000*Math.random());
                try {
                    Thread.sleep((long) (2000*Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    logger.warn("Thread is over",e);
                }
                //списание
                synchronized (a) {
                    if (a.getMoney() < transactionAmount) {
                        logger.info(cnt+" Недостаточно средств для списания c: " + a.toString());
                        continue;
                    } else {
                        a.setMoney(a.getMoney() - transactionAmount);
                        logger.info(cnt+" Сумма списания со счета id: " + a.getId() + " составляет: " + transactionAmount +" руб.");
                    }
                }
                //зачисление
                synchronized (b) {
                    b.setMoney(b.getMoney() + transactionAmount);
                    logger.info(cnt+" Произошло зачисление средств на счет id: " + b.getId() + " сумма зачисления составляет: " + transactionAmount +" руб.");
                }
            }
        } catch (Exception e) {
            logger.error("Some error", e);
        }
    }
    void totalForAccounts(){
        logger.info(a.toString() +" " +b.toString());
    }
}
