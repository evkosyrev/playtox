import java.util.Random;

public class Account {
    private String id;
    private Integer money;

    Account() {
        Random random = new Random();
        this.id = String.valueOf(random.nextInt(10000));
        this.money = 10000;
    }

    @Override
    public String toString() {
        return "Account{" + "id='" + id + '\'' + ", money=" + money + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Account account = (Account) o;

        if (!id.equals(account.id)) { return false; }
        return money.equals(account.money);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + money.hashCode();
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }
}
