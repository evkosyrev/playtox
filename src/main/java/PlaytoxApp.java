import java.util.concurrent.atomic.AtomicInteger;

public class PlaytoxApp {
    public static void main(String... args) {
        Account a = new Account();
        Account b = new Account();
        AtomicInteger atomicIntegerCounter = new AtomicInteger(0);
        Thread thread1 = new Thread(new TransferWorker(a, b, atomicIntegerCounter));
        Thread thread2 = new Thread(new TransferWorker(b, a, atomicIntegerCounter));
        thread1.setName("First thread");
        thread2.setName("Second thread");
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
